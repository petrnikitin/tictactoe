package com.example.tictactoe.controller;

import com.example.tictactoe.dto.MoveDto;
import com.example.tictactoe.dto.MoveResultDto;
import com.example.tictactoe.dto.PlayerDto;
import com.example.tictactoe.dto.type.PlayerType;
import com.example.tictactoe.dto.type.RoomState;
import com.example.tictactoe.service.MoveService;
import com.example.tictactoe.service.PlayerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TicTacToeTest {

    @Container
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer()
            .withPassword("inmemory")
            .withUsername("inmemory");

    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
    }

    @Autowired
    private PlayerService playerService;

    @Autowired
    private MoveService moveService;

    @Test
    public void createPlayersForTheGameTest() {
        PlayerDto playerXDto = new PlayerDto();
        PlayerDto savedPlayerXDto = playerService.createPlayer(playerXDto);
        PlayerDto expectedPlayerXDto = new PlayerDto(1L, PlayerType.X, 1L);
        Assertions.assertEquals(expectedPlayerXDto, savedPlayerXDto);

        PlayerDto playerODto = new PlayerDto(null, null, 1L);
        PlayerDto savedPlayerODto = playerService.createPlayer(playerODto);
        PlayerDto expectedPlayerODto = new PlayerDto(2L, PlayerType.O, 1L);
        Assertions.assertEquals(expectedPlayerODto, savedPlayerODto);
    }

    @Test
    public void playTheGameTest() {
        PlayerDto playerXDto = new PlayerDto();
        PlayerDto savedPlayerXDto = playerService.createPlayer(playerXDto);
        PlayerDto expectedPlayerXDto = new PlayerDto(1L, PlayerType.X, 1L);
        Assertions.assertEquals(expectedPlayerXDto, savedPlayerXDto);

        PlayerDto playerODto = new PlayerDto(null, null, 1L);
        PlayerDto savedPlayerODto = playerService.createPlayer(playerODto);
        PlayerDto expectedPlayerODto = new PlayerDto(2L, PlayerType.O, 1L);
        Assertions.assertEquals(expectedPlayerODto, savedPlayerODto);

        MoveDto firstMoveDto = new MoveDto(1L, 0, 0);
        MoveResultDto firstMoveResultDto = moveService.move(firstMoveDto);
        MoveResultDto firstExpectedMoveResultDto = new MoveResultDto(2L, RoomState.IN_PROCESS);
        Assertions.assertEquals(firstExpectedMoveResultDto, firstMoveResultDto);

        MoveDto secondMoveDto = new MoveDto(2L, 1, 1);
        MoveResultDto secondMoveResultDto = moveService.move(secondMoveDto);
        MoveResultDto secondExpectedMoveResultDto = new MoveResultDto(1L, RoomState.IN_PROCESS);
        Assertions.assertEquals(secondExpectedMoveResultDto, secondMoveResultDto);

        MoveDto thirdMoveDto = new MoveDto(1L, 0, 1);
        MoveResultDto thirdMoveResultDto = moveService.move(thirdMoveDto);
        MoveResultDto thirdExpectedMoveResultDto = new MoveResultDto(2L, RoomState.IN_PROCESS);
        Assertions.assertEquals(thirdExpectedMoveResultDto, thirdMoveResultDto);

        MoveDto fourthMoveDto = new MoveDto(2L, 1, 2);
        MoveResultDto fourthMoveResultDto = moveService.move(fourthMoveDto);
        MoveResultDto fourthExpectedMoveResultDto = new MoveResultDto(1L, RoomState.IN_PROCESS);
        Assertions.assertEquals(fourthExpectedMoveResultDto, fourthMoveResultDto);

        MoveDto fifthMoveDto = new MoveDto(1L, 0, 2);
        MoveResultDto fifthMoveResultDto = moveService.move(fifthMoveDto);
        MoveResultDto fifthExpectedMoveResultDto = new MoveResultDto(2L, RoomState.SUCCESS_X);
        Assertions.assertEquals(fifthExpectedMoveResultDto, fifthMoveResultDto);
    }
}
