package com.example.tictactoe.dto;

import com.example.tictactoe.dto.type.RoomState;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MoveResultDto {

    private Long nextPlayerId;
    private RoomState roomState;
}
