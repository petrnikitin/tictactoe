package com.example.tictactoe.dto;

import com.example.tictactoe.dto.type.PlayerType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDto {

    private Long id;
    private PlayerType type;
    private Long roomId;
}
