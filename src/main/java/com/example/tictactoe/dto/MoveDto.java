package com.example.tictactoe.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
public class MoveDto {

    private Long playerId;

    @Min(0)
    @Max(2)
    private int row;

    @Min(0)
    @Max(2)
    private int column;
}
