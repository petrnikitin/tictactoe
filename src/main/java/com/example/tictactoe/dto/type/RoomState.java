package com.example.tictactoe.dto.type;

public enum RoomState {

    NEW,
    IN_PROCESS,
    SUCCESS_X,
    SUCCESS_O,
    DRAW
}
