package com.example.tictactoe.dto.type;

public enum PlayerType {
    X,
    O
}
