package com.example.tictactoe.service;

import com.example.tictactoe.dao.entity.Move;
import com.example.tictactoe.dao.entity.Player;
import com.example.tictactoe.dao.entity.Room;
import com.example.tictactoe.dao.repository.MoveRepository;
import com.example.tictactoe.dao.repository.PlayerRepository;
import com.example.tictactoe.dto.MoveDto;
import com.example.tictactoe.dto.MoveResultDto;
import com.example.tictactoe.dto.type.PlayerType;
import com.example.tictactoe.dto.type.RoomState;
import com.example.tictactoe.exception.TicTacToeException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MoveService {
    private final MoveRepository moveRepository;
    private final PlayerRepository playerRepository;

    @Transactional
    public MoveResultDto move(MoveDto moveDto) {
        Optional<Player> playerOrt = playerRepository.findById(moveDto.getPlayerId());
        if (playerOrt.isEmpty()) {
            log.error("Player not found by id: {}", moveDto.getPlayerId());
            throw new TicTacToeException(String.format("Player not found by id: %s", moveDto.getPlayerId()));
        }
        Player player = playerOrt.get();
        Room room = player.getRoom();
        if (!RoomState.IN_PROCESS.equals(room.getState())) {
            log.error("Game is over or incorrect state of roomId: {}", room.getId());
            throw new TicTacToeException(String.format("Game is over or incorrect state of roomId: %s", room.getId()));
        }
        if (!room.getNextPlayer().equals(player)) {
            log.error("Wrong turn for player with id: {}", player.getId());
            throw new TicTacToeException(String.format("Wrong turn for player with id: %s", player.getId()));
        }
        List<Move> allPreviousMoves = getAllPreviousMoves(room);
        checkOnDuplicates(moveDto, allPreviousMoves);
        boolean isWinningMove = isWinningMove(moveDto, player.getId(), allPreviousMoves);
        createMove(moveDto);

        Player nextPlayer = playerRepository.getPlayerByRoomAndIdNot(room, player.getId());
        room.setNextPlayer(nextPlayer);
        if (isWinningMove) {
            RoomState roomState = PlayerType.X.equals(player.getType()) ? RoomState.SUCCESS_X : RoomState.SUCCESS_O;
            room.setState(roomState);
        } else if (allPreviousMoves.size() == 8) {
            room.setState(RoomState.DRAW);
        }
        return new MoveResultDto(nextPlayer.getId(), room.getState());
    }

    private List<Move> getAllPreviousMoves(Room room) {
        List<Long> playerIdsInRoom = playerRepository.getPlayersByRoom(room)
                .stream()
                .map(Player::getId).toList();
        return moveRepository.getMovesByPlayerIdIn(playerIdsInRoom);
    }

    private void checkOnDuplicates(MoveDto moveDto, List<Move> allPreviousMoves) {
        Optional<Move> duplicatedMoveOpt = allPreviousMoves.stream()
                .filter(move -> move.getColumn() == moveDto.getColumn() && move.getRow() == moveDto.getRow())
                .findFirst();
        if (duplicatedMoveOpt.isPresent()) {
            log.error("Duplicated move was found by moveId: {}", duplicatedMoveOpt.get().getId());
            throw new TicTacToeException(String.format("Duplicated move was found by moveId: %s", duplicatedMoveOpt.get().getId()));
        }
    }

    private boolean isWinningMove(MoveDto moveDto, Long playerId, List<Move> allPreviousMoves) {
        List<Move> previousMovesByCurrentPlayer = allPreviousMoves.stream()
                .filter(move -> playerId.equals(move.getPlayerId()))
                .toList();
        int row = moveDto.getRow();
        int column = moveDto.getColumn();

        if (previousMovesByCurrentPlayer.size() < 2) {
            return false;
        }

        boolean lineByRow = previousMovesByCurrentPlayer.stream()
                .filter(move -> row == move.getRow())
                .count() == 2;
        boolean lineByColumn = previousMovesByCurrentPlayer.stream()
                .filter(move -> column == move.getColumn())
                .count() == 2;

        boolean lineByDiagonal;
        if (isDiagonal(row, column)) {
            if (row == column) {
                boolean lineByLeftDiagonal = false;
                if (row == 1) {
                    lineByLeftDiagonal = previousMovesByCurrentPlayer.stream()
                            .filter(move -> Math.abs(move.getRow() - move.getColumn()) == 2)
                            .count() == 2;
                }
                boolean lineByRightDiagonal = previousMovesByCurrentPlayer.stream()
                        .filter(move -> move.getRow() == move.getColumn())
                        .count() == 2;
                lineByDiagonal = lineByLeftDiagonal || lineByRightDiagonal;
            } else {
                lineByDiagonal = previousMovesByCurrentPlayer.stream()
                        .filter(move ->
                                Math.abs(move.getRow() - move.getColumn()) == 2 ||
                                        (move.getRow() == 1 && move.getColumn() == 1))
                        .count() == 2;
            }
        } else {
            lineByDiagonal = false;
        }
        return lineByRow || lineByColumn || lineByDiagonal;
    }

    private boolean isDiagonal(int row, int column) {
        return row == column || Math.abs(row - column) == 2;
    }

    private void createMove(MoveDto moveDto) {
        Move move = new Move();
        move.setColumn(moveDto.getColumn());
        move.setRow(moveDto.getRow());
        move.setPlayerId(moveDto.getPlayerId());
        moveRepository.save(move);
    }
}
