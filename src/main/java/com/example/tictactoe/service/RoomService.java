package com.example.tictactoe.service;

import com.example.tictactoe.dao.entity.Room;
import com.example.tictactoe.dao.repository.RoomRepository;
import com.example.tictactoe.dto.type.RoomState;
import com.example.tictactoe.exception.TicTacToeException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class RoomService {
    private final RoomRepository roomRepository;

    @Transactional(readOnly = true)
    public Room getRoom(Long id) {
        Optional<Room> roomOpt = roomRepository.findById(id);
        if (roomOpt.isEmpty()) {
            log.error("Cant find room by roomId: {}", id);
            throw new TicTacToeException(String.format("Cant find room by roomId: %s", id));
        }
        return roomOpt.get();
    }

    @Transactional
    public Room createRoom() {
        Room room = new Room();
        room.setState(RoomState.NEW);
        return roomRepository.save(room);
    }
}
