package com.example.tictactoe.service;

import com.example.tictactoe.dao.entity.Player;
import com.example.tictactoe.dao.entity.Room;
import com.example.tictactoe.dao.repository.PlayerRepository;
import com.example.tictactoe.dto.PlayerDto;
import com.example.tictactoe.dto.type.PlayerType;
import com.example.tictactoe.dto.type.RoomState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlayerService {
    private final ModelMapper modelMapper;
    private final PlayerRepository playerRepository;
    private final RoomService roomService;

    @Transactional
    public PlayerDto createPlayer(PlayerDto playerDto) {
        Room room;
        boolean isFirstPlayer = playerDto.getRoomId() == null;
        if (isFirstPlayer) {
            room = roomService.createRoom();
        } else {
            room = roomService.getRoom(playerDto.getRoomId());
            room.setState(RoomState.IN_PROCESS);
        }
        Player player = new Player();
        player.setRoom(room);
        player.setType(isFirstPlayer ? PlayerType.X : PlayerType.O);
        playerRepository.save(player);

        if (isFirstPlayer) {
            room.setNextPlayer(player);
        }
        return mapEntityToDto(player);
    }

    private PlayerDto mapEntityToDto(Player player) {
        return modelMapper.map(player, PlayerDto.class);
    }
}
