package com.example.tictactoe.dao.repository;


import com.example.tictactoe.dao.entity.Player;
import com.example.tictactoe.dao.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlayerRepository extends CrudRepository<Player, Long> {

    Player getPlayerByRoomAndIdNot(Room room, Long playerId);

    List<Player> getPlayersByRoom(Room room);
}
