package com.example.tictactoe.dao.repository;

import com.example.tictactoe.dao.entity.Move;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import javax.sql.rowset.CachedRowSet;
import java.util.Collection;
import java.util.List;

public interface MoveRepository extends CrudRepository<Move, Long> {

    List<Move> getMovesByPlayerIdIn(Collection<Long> playerId);
}
