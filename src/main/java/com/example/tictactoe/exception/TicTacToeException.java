package com.example.tictactoe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class TicTacToeException extends RuntimeException {

    public TicTacToeException(String message) {
        super(message);
    }




}
