package com.example.tictactoe.controller;

import com.example.tictactoe.dto.MoveDto;
import com.example.tictactoe.dto.MoveResultDto;
import com.example.tictactoe.dto.PlayerDto;
import com.example.tictactoe.service.MoveService;
import com.example.tictactoe.service.PlayerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class MainController {
    private final PlayerService playerService;
    private final MoveService moveService;

    @PostMapping(value = "/create/player", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDto> createPlayer(@RequestBody PlayerDto playerDto) {
        if (playerDto.getId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(playerService.createPlayer(playerDto));
    }

    @PostMapping(value = "/move", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MoveResultDto> move(@RequestBody MoveDto moveDto) {
        return ResponseEntity.ok(moveService.move(moveDto));
    }
}
