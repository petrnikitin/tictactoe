FROM openjdk:19
COPY target/tictactoe-0.0.1-SNAPSHOT.jar tictactoe.jar
ENTRYPOINT ["java","-jar","/tictactoe.jar"]