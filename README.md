## tic-tac-toe

## Running the application

To run this demo - run docker-compose.yml
You will need docker, java 19, idea.

## Description

This service implements a game of tic-tac-toe.

## API:

- `/api/v1/create/player` - create a player and a room (gaming board) if it doesn't exist. For this, you need to send an empty object to create the first player. A player will be created
who start game with X. Then you need to add a second player to the game using
the returned roomId.  
    Request JSON body example for the first player in the game:
    ```
    {
    }
    ```
    Response JSON body:
    ```
    {
        "id": 1,
        "type": "X",
        "roomId": 1
    }
    ```
  Request JSON body example for the second player in the game:
    ```
    {
        "roomId": 1
    }
    ```
  Response JSON body:
    ```
    {
        "id": 2,
        "type": "O",
        "roomId": 1
    }
    ```
- `/api/v1/move` - to make a move. Accepts the player's ID and coordinates on the game board.
The returned object is a JSON with the ID of the next player and the state of the room.
The method also calculates who the next move will belong to, checks the state of the room -and responsible for
finding the winning move algorithm.  
  Request JSON body example:
    ```
    {
        "playerId": 1,
        "row": 0,
        "column": 0
    }
    ```
  Response JSON body:
    ```
    {
        "nextPlayerId": 2,
        "roomState": "IN_PROGRESS"
    }
    ```

